from threading import Thread
import freecoin
import time


class FreecoinThread(Thread):
    def run(self):
        freecoin.AppInit(["freecoin"])

t = FreecoinThread()
t.start()

while True:
    time.sleep(1)
    print(freecoin.cvar.nBestHeight, freecoin.GetDifficulty())
