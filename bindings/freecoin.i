%module freecoin

/* Headers and declarations for our output c++ program */
%{

#if defined(SWIGRUBY)
        #undef _ /* nice one ruby.. */
#endif

#include "headers.h"

extern double GetDifficulty();
%}

/* Language specific typemaps */
#if defined(SWIGPYTHON)
  %include "pypre.i"
#elif defined(SWIGRUBY)
  %include "rbpre.i"
#elif defined(SWIGJAVA)
  %include "javapre.i"
#elif defined(SWIGPERL)
  %include "perlpre.i"
#endif

/* INCLUDES */
/* GetDifficulty doesnt have a header file... */
double GetDifficulty();

/* Ignore some problematic variables */
%ignore BroadcastTransaction;
%ignore fUseUPnP;
%ignore cs_main;
%ignore cs_mapRequestCount;
%ignore cs_mapAddressBook;
%ignore cs_mapWallet;
%ignore cs_mapKeys;

/* We have problems with some CDataStream constructors, but since its tricky to ignore them we just
   ignore the full class for now */
%ignore CDataStream;
%include <serialize.h>

/* Funny problem with CBlockIndex::nMedianTimeSpan */
%ignore CBlockIndex::nMedianTimeSpan;
%include <main.h>

%include <init.h>
