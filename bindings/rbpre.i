
/* TYPEMAPS */
/* typemap for argc/argv to ruby array */
%typemap(in) (int argc, char *argv[]) {
  /* Check if is a list */
 /* Get the length of the array */
 int size = RARRAY($input)->len; 
 int i;
 $1 = size;
 $2 = (char **) malloc((size+1)*sizeof(char *));
 /* Get the first element in memory */
 VALUE *ptr = RARRAY($input)->ptr; 
 for (i=0; i < size; i++, ptr++)
 /* Convert Ruby Object String to char* */
 $2[i]= STR2CSTR(*ptr); 
 $2[i]=NULL; /* End of list */
}

%typemap(freearg) (int argc, char *argv[]) {
  free((char *) $2);
}


