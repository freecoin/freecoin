# ruby
swig -D__WXDEBUG__ -DNOPCH -DFOURWAYSSE2 -DUSE_SSL -c++ -java -I.. bitcoin.i
g++ -c -O2 -Wno-invalid-offsetof -Wformat -fno-strict-aliasing -g -UUSE_UPNP -D__WXDEBUG__ -DNOPCH -DFOURWAYSSE2 -DUSE_SSL -msse2 -O3 -march=amdfam10 -I.. -I/usr/lib/jvm/java-6-sun-1.6.0.20/include -I/usr/lib/jvm/java-6-sun-1.6.0.20/include/linux -o ../obj/bitcoin_wrap.o bitcoin_wrap.cxx
g++ -UUSE_UPNP -O2 -Wno-invalid-offsetof -fno-strict-aliasing -Wformat -g -D__WXDEBUG__ -DNOPCH -DFOURWAYSSE2 -DUSE_SSL -shared -export-dynamic -I.. -I/usr/lib/jvm/java-6-sun-1.6.0.20/include -I/usr/lib/jvm/java-6-sun-1.6.0.20/include/linux -o libbitcoin.so ../obj/bitcoin_wrap.o ../obj/nogui/util.o ../obj/nogui/script.o ../obj/nogui/db.o ../obj/nogui/net.o ../obj/nogui/irc.o ../obj/nogui/main.o ../obj/nogui/rpc.o ../obj/nogui/init.o ../cryptopp/obj/sha.o ../cryptopp/obj/cpu.o ../obj/sha256.o -Wl,-Bstatic -l boost_system -l boost_filesystem -l boost_program_options -l boost_thread -l db_cxx -l ssl -l crypto -Wl,-Bdynamic -l gthread-2.0 -l z -l dl
mv *java libbitcoin.so java/

# ruby
